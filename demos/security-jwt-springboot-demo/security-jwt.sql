/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : security-jwt

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2020-07-01 11:01:36
*/
DROP DATABASE IF EXISTS  `security-jwt`;
CREATE DATABASE  `security-jwt` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `security-jwt`;

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for authority
-- ----------------------------
DROP TABLE IF EXISTS `authority`;
CREATE TABLE `authority` (
  `mid` varchar(32) NOT NULL COMMENT '主键id',
  `name` varchar(64) NOT NULL COMMENT '键',
  `value` varchar(64) NOT NULL COMMENT '值',
  `organize` varchar(64) NOT NULL COMMENT '组织机构',
  PRIMARY KEY (`mid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='权限表';

-- ----------------------------
-- Records of authority
-- ----------------------------

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `mid` varchar(32) NOT NULL COMMENT '主键id',
  `name` varchar(64) NOT NULL COMMENT '角色名称',
  `value` varchar(128) NOT NULL COMMENT '值',
  `organize` varchar(64) NOT NULL COMMENT '机构',
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否可用 1:是 0:否',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否删除 1:是 0:否',
  `create_time` timestamp(3) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp(3) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`mid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色表';

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', '超级管理员', 'ADMIN', 'SURGE', '1', '0', '2018-09-17 16:48:10.000', '2019-12-17 14:50:52.000');
INSERT INTO `role` VALUES ('2', '游客', 'TOURIST', 'SURGE', '1', '0', '2019-12-17 14:50:47.000', '2019-12-17 14:50:54.000');
INSERT INTO `role` VALUES ('3', '普通用户', 'USER', 'SURGE', '1', '0', '2019-12-17 14:50:49.000', '2019-12-17 14:50:56.000');

-- ----------------------------
-- Table structure for role_authority_mapping
-- ----------------------------
DROP TABLE IF EXISTS `role_authority_mapping`;
CREATE TABLE `role_authority_mapping` (
  `mid` varchar(32) NOT NULL COMMENT '主键id',
  `role_id` varchar(32) NOT NULL COMMENT '角色id',
  `authority_id` varchar(32) NOT NULL COMMENT '权限id',
  PRIMARY KEY (`mid`),
  KEY `idx_role_id` (`role_id`) USING BTREE,
  KEY `idx_authority_id` (`authority_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='权限角色映射表';

-- ----------------------------
-- Records of role_authority_mapping
-- ----------------------------

-- ----------------------------
-- Table structure for user_account
-- ----------------------------
DROP TABLE IF EXISTS `user_account`;
CREATE TABLE `user_account` (
  `mid` varchar(32) NOT NULL COMMENT '主键id',
  `username` varchar(64) NOT NULL COMMENT '魔咕号，只能设置一次',
  `password` varchar(128) DEFAULT NULL COMMENT '密码',
  `phone` varchar(13) DEFAULT NULL COMMENT '电话号',
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否启用 1:启用 0:未启用',
  `is_unlocked` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否锁定 0:锁定 1:未锁定',
  `is_unExpired` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否过期 0：过期 1：未过期',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否删除 0：未删除 1：删除',
  `create_time` timestamp(3) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp(3) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`mid`),
  UNIQUE KEY `ux_mogu_num` (`username`) USING BTREE,
  UNIQUE KEY `ux_phone` (`phone`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户账户信息表';

-- ----------------------------
-- Records of user_account
-- ----------------------------
INSERT INTO `user_account` VALUES ('111111111111111111', 'admin', '$2a$10$nGjRH.9u52kcgtaBt/Svqe3XL40M2hRzk8matpflMkR5GjFL5ipcK', '456', '1', '1', '1', '0', '2019-12-17 14:50:19.000', '2020-02-28 11:14:41.315');
INSERT INTO `user_account` VALUES ('111111111111111112', 'tourist', '$2a$10$nGjRH.9u52kcgtaBt/Svqe3XL40M2hRzk8matpflMkR5GjFL5ipcK', '123', '1', '1', '1', '0', '2019-12-17 14:50:22.000', '2018-12-29 17:28:38.000');
INSERT INTO `user_account` VALUES ('12000000001', 'user1', '$2a$10$nGjRH.9u52kcgtaBt/Svqe3XL40M2hRzk8matpflMkR5GjFL5ipcK', '12000000001', '1', '1', '1', '0', '2020-01-06 17:42:14.530', '2020-01-06 17:42:14.530');
INSERT INTO `user_account` VALUES ('12000000002', 'user2', '$2a$10$nGjRH.9u52kcgtaBt/Svqe3XL40M2hRzk8matpflMkR5GjFL5ipcK', '12000000002', '1', '1', '1', '0', '2020-01-06 17:44:09.361', '2020-01-06 17:44:09.361');
INSERT INTO `user_account` VALUES ('12000000003', 'user3', '$2a$10$nGjRH.9u52kcgtaBt/Svqe3XL40M2hRzk8matpflMkR5GjFL5ipcK', '12000000003', '1', '1', '1', '0', '2020-01-06 17:51:08.913', '2020-01-06 17:51:08.913');

-- ----------------------------
-- Table structure for user_binding
-- ----------------------------
DROP TABLE IF EXISTS `user_binding`;
CREATE TABLE `user_binding` (
  `mid` varchar(43) NOT NULL COMMENT '主键id',
  `account_id` varchar(32) NOT NULL COMMENT '用户id',
  `channel` int(1) NOT NULL COMMENT '1:微信 2:网易IM',
  `binding_key` varchar(20) NOT NULL COMMENT '绑定键名',
  `binding_value` varchar(128) NOT NULL COMMENT '绑定值',
  `create_time` timestamp(3) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`mid`),
  KEY `ux_accid_bingingkey_bindingvalue` (`account_id`,`binding_key`,`binding_value`) USING BTREE,
  KEY `idx_account_id` (`account_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户外部平台绑定表';

-- ----------------------------
-- Records of user_binding
-- ----------------------------
INSERT INTO `user_binding` VALUES ('1156811542696824833', '111111111111111111', '1', 'unionId', 'oouhX0juVT3Vli4evLxU-8NV28D', '2019-08-01 14:18:59.000');

-- ----------------------------
-- Table structure for user_role_mapping
-- ----------------------------
DROP TABLE IF EXISTS `user_role_mapping`;
CREATE TABLE `user_role_mapping` (
  `mid` varchar(32) NOT NULL COMMENT '主键id',
  `account_id` varchar(32) NOT NULL COMMENT '账户id',
  `role_id` varchar(32) NOT NULL COMMENT '角色id',
  PRIMARY KEY (`mid`),
  KEY `idx_user_id` (`account_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户角色映射表';

-- ----------------------------
-- Records of user_role_mapping
-- ----------------------------
INSERT INTO `user_role_mapping` VALUES ('1', '111111111111111111', '1');
INSERT INTO `user_role_mapping` VALUES ('1214119977436839936', '12000000001', '3');
INSERT INTO `user_role_mapping` VALUES ('1214120459072962560', '12000000002', '3');
INSERT INTO `user_role_mapping` VALUES ('1214122218809982976', '12000000003', '3');


-- ----------------------------
-- Table structure for user_info
-- ----------------------------
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `account_id` bigint(20) NOT NULL COMMENT '账户id',
  `nickname` varchar(64) NOT NULL COMMENT '昵称',
  `sex` char(1) NOT NULL COMMENT 'M:男 F:女 N:未知',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `head_portrait` varchar(256) DEFAULT NULL COMMENT '头像',
  `address_code` varchar(8) DEFAULT NULL COMMENT '地址',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `qq_number` varchar(16) DEFAULT NULL COMMENT 'QQ号',
  `wechat_number` varchar(32) DEFAULT NULL COMMENT '微信号',
  `personalized_signature` varchar(256) DEFAULT NULL COMMENT '个性签名',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:否 1:是',
  `create_time` timestamp(3) NULL DEFAULT NULL,
  `update_time` timestamp(3) NULL DEFAULT NULL,
  `liveness` int(7) NOT NULL DEFAULT '0' COMMENT '活跃度',
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_id` (`account_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of user_info
-- ----------------------------
BEGIN;
INSERT INTO `user_info` VALUES (1206851093984321939, 111111111111111111, '系统管理员', 'M', '2020-01-06', 'https://cube.elemecdn.com/0/88/03b0d39583f48206768a7534e55bcpng.png', '210101', '1074723451@qq.com', '1074723451', 'pp210510', 'QIPENGPAI&&BISHUAI', 0, '2019-12-17 16:18:18.000', '2021-03-28 13:22:23.421', 0);
INSERT INTO `user_info` VALUES (1206851093989031936, 111111111111111112, '游客', 'M', '1969-12-31', 'https://cube.elemecdn.com/0/88/03b0d39583f48206768a7534e55bcpng.png', NULL, 'adf@163.com', '652366325', '3652556', 'personalizedSignature', 1, '2019-12-17 02:18:17.675', '2020-10-16 15:44:17.277', 0);
INSERT INTO `user_info` VALUES (1206851093989031937, 12000000001, '澎湃', 'M', '2020-05-15', 'https://cube.elemecdn.com/0/88/03b0d39583f48206768a7534e55bcpng.png', NULL, NULL, NULL, NULL, NULL, 1, '2020-05-15 11:08:41.000', '2020-10-16 14:49:34.813', 0);
INSERT INTO `user_info` VALUES (1261201525775147008, 12000000002, '昵称382093', 'F', NULL, 'https://cube.elemecdn.com/0/88/03b0d39583f48206768a7534e55bcpng.png ', NULL, NULL, NULL, NULL, NULL, 1, '2020-05-15 15:47:30.396', '2020-10-16 15:35:43.832', 0);
INSERT INTO `user_info` VALUES (1261210455939096576, 12000000003, '昵称176705', 'F', NULL, 'https://cube.elemecdn.com/0/88/03b0d39583f48206768a7534e55bcpng.png', NULL, NULL, NULL, NULL, NULL, 1, '2020-05-15 16:22:59.512', '2020-10-16 15:39:00.060', 0);
INSERT INTO `user_info` VALUES (1316276123314823168, 1316276122241081344, 'bs', 'F', '1993-01-19', 'https://cube.elemecdn.com/0/88/03b0d39583f48206768a7534e55bcpng.png', NULL, '123123312@qq.com', '121413413', '123131233', '爱你哟', 0, '2020-10-14 15:14:17.685', '2021-03-28 13:26:49.979', 0);
COMMIT;
