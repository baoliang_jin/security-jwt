package top.surgeqi.security.demo.bean.po;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import java.time.LocalDateTime;

/**
 * <p><em>Created by qipp on 2020/10/1 11:11</em></p>
 * 角色
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Data
public class Role implements GrantedAuthority, Comparable<Role> {
    /**
     * 主键id
     */
    @TableId
    private Long mid;

    /**
     * 角色名称
     */
    private String name;

    /**
     * 值
     */
    private String value;

    /**
     * 机构
     */
    private String organize;

    /**
     * 是否可用 1:是 0:否
     */
    private Boolean isEnabled;

    /**
     * 是否删除 1:是 0:否
     */
    private Boolean isDelete;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 拼接角色
     * <p>ROLE_ + {@code this.organize} + _ + {@code this.value}</p>
     * @return java.lang.String 完整角色名称
     * @author qipp
     */
    @Override
    public String getAuthority() {
        return "ROLE_" + this.organize +
                "_" +
                this.value;
    }

    /**
     * 判断相同方法
     * @param o 对比角色对象
     * @return int 0、1、-1
     * @author qipp
     */
    @Override
    public int compareTo(Role o) {
        // 判断id是否相同
        if (this.mid.equals(o.getMid())) {
            return 0;
        }
        if(this.mid > o.getMid()){
            return 1;
        }
        return -1;
    }
}
