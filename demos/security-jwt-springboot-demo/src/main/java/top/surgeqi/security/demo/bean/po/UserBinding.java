package top.surgeqi.security.demo.bean.po;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import java.util.Date;

/**
 * <p><em>Created by qipp on 2020/10/1 11:11</em></p>
 * 用户绑定账户
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Data
public class UserBinding {
    /**
     * 主键id
     */
    @TableId
    private Long mid;

    /**
     * 用户id
     */
    private Long accountId;

    /**
     * 1:微信 2:网易IM
     */
    private Integer channel;

    /**
     * 绑定键名
     */
    private String bindingKey;

    /**
     * 绑定值
     */
    private String bindingValue;

    /**
     * 创建时间
     */
    private Date createTime;
}
