package top.surgeqi.security.demo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import top.surgeqi.security.demo.bean.po.UserBinding;

/**
 * <p><em>Created by qipp on 2020/7/1 15:36</em></p>
 * 用户绑定信息 Mapper 接口
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Repository
public interface UserBindingMapper extends BaseMapper<UserBinding> {

    /**
     * 获取绑定微信的用户ID
     * @param unionId  微信ID
     * @return org.surge.oauth2common.model.UserBinding
     * @author qipp
     */
    UserBinding selectWeChatUnionId(String unionId);
}