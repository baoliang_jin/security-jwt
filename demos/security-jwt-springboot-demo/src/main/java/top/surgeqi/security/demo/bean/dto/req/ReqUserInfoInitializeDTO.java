package top.surgeqi.security.demo.bean.dto.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * <p><em>Created by qipp on 2020/10/6 9:41 上午</em></p>
 * 用户完善修改信息 DTO
 *
 * @author <a href ="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Data
@ApiModel(value = "新增用户账户")
public class ReqUserInfoInitializeDTO {

    @NotNull(message = "用户账户id为空")
    @ApiModelProperty(value = "账户id")
    private Long accountId;

    @Length(max = 64)
    @ApiModelProperty("昵称")
    private String nickname;

    @ApiModelProperty("性别")
    private String sex;

    @ApiModelProperty("头像")
    private String headPortrait;

    @ApiModelProperty(value = "个性签名")
    private String personalizedSignature;

    @ApiModelProperty(value = "生日")
    private Date birthday;
}
