package top.surgeqi.security.demo.bean.dto.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * <p><em>Created by qipp on 2020/10/13 21:30</em></p>
 * 用户注册 DTO
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Data
@ApiModel(value = "用户注册")
public class ReqUserAccountInitializeDTO {

    @NotNull(message = "用户唯一账号为空")
    @ApiModelProperty(value = "用户唯一账号")
    private String username;

    @Length(max = 64)
    @ApiModelProperty("昵称")
    private String nickname;

    @NotNull(message = "手机号为空")
    @ApiModelProperty("手机号")
    private String phone;

    @NotNull(message = "手机验证码为空")
    @ApiModelProperty("手机验证码")
    private String phoneCode;

    @NotNull(message = "密码为空")
    @ApiModelProperty(value = "密码")
    private String password;
}
