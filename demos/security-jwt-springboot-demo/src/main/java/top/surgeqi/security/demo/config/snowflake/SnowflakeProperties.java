package top.surgeqi.security.demo.config.snowflake;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 雪花算法配置类
 *
 * @author qipp
 * @date 2020/5/15 10:29
 * @since 1.0.1
 */
@Data
@ConfigurationProperties(SnowflakeProperties.PREFIX)
public class SnowflakeProperties {

    /**
     * 认证配置前缀{@value}
     */
    public static final String PREFIX = "surge.snowflake";

    /**
     * 终端ID
     */
    private Long workerId = 1L;

    /**
     * 数据中心ID
     */
    private Long dataCenterId = 1L;
}
