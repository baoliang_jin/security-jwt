package top.surgeqi.security.demo.bean.dto.res;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * <p><em>Created by qipp on 2020/10/9 9:41 上午</em></p>
 * 获取用户详情信息 DTO
 *
 * @author <a href ="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Data
@ApiModel(value = "获取用户详情信息")
public class ResGetUserInfoDTO {

    @JsonSerialize(using= ToStringSerializer.class)
    @ApiModelProperty(value = "用户账户Id")
    private Long accountId;

    @JsonSerialize(using=ToStringSerializer.class)
    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "昵称")
    private String nickname;

    @ApiModelProperty(value = "M:男 F:女 N:未知")
    private String sex;

    @ApiModelProperty(value = "生日")
    private LocalDateTime birthday;

    @ApiModelProperty(value = "头像")
    private String headPortrait;

    @ApiModelProperty(value = "地址")
    private String addressCode;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "QQ号")
    private String qqNumber;

    @ApiModelProperty(value = "微信号")
    private String wechatNumber;

    @ApiModelProperty(value = "个性签名")
    private String personalizedSignature;

    @ApiModelProperty(value = "是否已删除0:否 1:是")
    private Boolean isDelete;

    @ApiModelProperty(value = "注册时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "电话号")
    private String phone;

    @ApiModelProperty(value = "用户账号")
    private String username;
}
