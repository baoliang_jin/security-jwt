package top.surgeqi.security.demo.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import top.surgeqi.security.demo.bean.po.UserAccount;
import top.surgeqi.security.demo.bean.core.Result;
import top.surgeqi.security.demo.dao.UserAccountMapper;

import javax.annotation.Resource;

/**
 * <p><em>Created by qipp on 2020/5/27 10:38</em></p>
 * 测试控制器
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@RestController
@Api(tags = {"测试接口相关"} )
public class TestController {

    /**
     * 测试
     */
    @Resource
    private UserAccountMapper userAccountMapper;

    /**
     * 测试接口
     * @return top.surgeqi.security.demo.bean.po.UserAccount 用户账户对象
     * @author qipp
     */
    @GetMapping("/hello")
    @ApiOperation(value = "测试接口hello")
    public Result<UserAccount> hello(){
        return Result.succeed(userAccountMapper.selectById("111111111111111111"));
    }

    /**
     * 测试接口2
     * @return top.surgeqi.security.demo.bean.po.UserAccount 用户账户对象
     * @author qipp
     */
    @GetMapping("/helloSuccess")
    @ApiOperation(value = "测试接口helloSuccess")
    public Result<Object> helloSuccess(){
        return Result.succeed();
    }
}
