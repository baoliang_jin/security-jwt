package top.surgeqi.security.demo.config.snowflake;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 雪花算法配置类Config
 *
 * @author qipp
 * @date 2020/5/15 10:28
 * @since 1.0.1
 */
@Slf4j
@Configuration
@EnableConfigurationProperties(value = {SnowflakeProperties.class})
public class SnowflakeConfig {

    /**
     * 雪花算法配置
     */
    private final SnowflakeProperties snowflakeProperties;

    public SnowflakeConfig(SnowflakeProperties snowflakeProperties) {
        this.snowflakeProperties = snowflakeProperties;
    }

    /**
     * 配置雪花算法
     * @return cn.hutool.core.lang.Snowflake
     * @author qipp
     * @date 2020/5/15 10:35
     */
    @Bean
    public Snowflake snowflake() {
        // 为终端ID默认值 1
        Long workerId = snowflakeProperties.getWorkerId();
        if (workerId == null) {
            workerId = 1L;
        }

        // 数据中心ID默认值 1
        Long dataCenterId = snowflakeProperties.getDataCenterId();
        if (dataCenterId == null) {
            dataCenterId = 1L;
        }
        log.info("配置雪花算法 workerId -> {}, dataCenterId -> {}", workerId, dataCenterId);
        return IdUtil.createSnowflake(workerId, dataCenterId);
    }
}
