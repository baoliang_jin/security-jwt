package top.surgeqi.security.demo.bean.core;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * <p><em>Created by qipp on 2020/10/15 9:32 下午</em></p>
 * 分页入参
 * T 请求参数
 * P 分页标识位
 *
 * @author <a href ="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Data
@ApiModel(value = "分页入参")
public class ReqPage<T, P>{

    @NotNull
    @Min(1)
    @ApiModelProperty("查询第几页")
    private Long page;

    @NotNull
    @ApiModelProperty("每页条数")
    private Long rows;

    @ApiModelProperty("分页标志位")
    private P flag;

    @Valid
    @ApiModelProperty("分页参数")
    private T parameter;
}
