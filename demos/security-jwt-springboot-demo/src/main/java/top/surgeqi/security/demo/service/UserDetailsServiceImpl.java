package top.surgeqi.security.demo.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import top.surgeqi.security.demo.bean.po.UserAccount;
import top.surgeqi.security.jwt.config.AbstractUserDetailsService;

/**
 * <p><em>Created by qipp on 2020/7/1 14:40</em></p>
 * 用户详情Service实现类
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Service
public class UserDetailsServiceImpl extends AbstractUserDetailsService {

    /**
     * 用户账户Service
     */
    private final UserAccountService userAccountService;

    public UserDetailsServiceImpl(UserAccountService userAccountService) {
        this.userAccountService = userAccountService;
    }

    /**
     * 调用用户账户service根据用户名查询用户账户详情信息
     *
     * @param username 用户名
     * @return org.springframework.security.core.userdetails.UserDetails
     * @author qipp
     */
    @Override
    public UserDetails selectByUserName(String username) {
        return userAccountService.selectByUserName(username);
    }

    /**
     * 调用用户账户service组合用户账户对象与角色权限
     *
     * @param userDetails 用户账户详情信息
     * @return org.springframework.security.core.userdetails.UserDetails
     * @author qipp
     */
    @Override
    public UserDetails composeUserDetailsAndAuthority(UserDetails userDetails) {
        return userAccountService.composeUserDetailsAndAuthority((UserAccount) userDetails);
    }

}
