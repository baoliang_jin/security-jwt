package top.surgeqi.security.demo.service;


import top.surgeqi.security.demo.bean.core.ReqPage;
import top.surgeqi.security.demo.bean.core.ResPage;
import top.surgeqi.security.demo.bean.core.Result;
import top.surgeqi.security.demo.bean.dto.req.ReqUpdateUserInfoDTO;
import top.surgeqi.security.demo.bean.dto.res.ResGetUserInfoDTO;
import top.surgeqi.security.demo.bean.po.UserInfo;

import java.time.LocalDateTime;

/**
 * 用户信息服务类
 *
 * @author qipp
 * @date 2020-05-15
 * @since 1.0.1
 */
public interface UserInfoService {

    /**
     * 用户基础信息初始化
     *
     * @param accountId 用户账户ID
     * @param nickname  用户昵称
     * @return org.surge.oauth2common.model.Result
     * @author qipp
     * @date 2020/5/15 11:58
     */
    Result<Object> initialize(Long accountId, String nickname);

    /**
     * 获取用户详情信息
     *
     * @param accountId 用户账户ID
     * @return top.surgeqi.security.demo.bean.result.Result<top.surgeqi.security.demo.bean.dto.res.ResGetUserInfoDTO>
     * @author qipp
     */
    Result<ResGetUserInfoDTO> getUserInfoDTO(Long accountId);

    /**
     * 获取用户详情信息
     *
     * @param accountId 用户账户ID
     * @return top.surgeqi.security.demo.bean.result.Result<top.surgeqi.security.demo.bean.dto.res.ResGetUserInfoDTO>
     * @author qipp
     */
    UserInfo getUserInfo(Long accountId);

    /**
     * 用户更新信息
     *
     * @param reqUpdateUserInfoDTO 用户更新信息 DTO
     * @return top.surgeqi.security.demo.bean.result.Result<java.lang.Object>
     * @author qipp
     */
    Result<Object> updateUserInfo(ReqUpdateUserInfoDTO reqUpdateUserInfoDTO);


    /**
     * 逻辑删除用户
     *
     * @param userInfo 用户账户ID 删除状态
     * @author qipp
     * @return top.surgeqi.security.demo.bean.result.Result<java.lang.Object>
     */
    Result<Object> logicDeleteUser(UserInfo userInfo);

    /**
     * 获取用户列表
     *
     * @param reqUpdateUserInfoDTO 获取用户列表 DTO 分页参数 筛选参数
     * @return top.surgeqi.security.demo.bean.core.Result<java.util.List < top.surgeqi.security.demo.bean.dto.res.ResGetUserInfoDTO>>
     * @author qipp
     */
    Result<ResPage<ResGetUserInfoDTO, LocalDateTime>> getUserInfoList(ReqPage<ReqUpdateUserInfoDTO, LocalDateTime> reqUpdateUserInfoDTO);

}
