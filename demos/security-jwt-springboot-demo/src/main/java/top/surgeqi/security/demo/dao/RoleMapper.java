package top.surgeqi.security.demo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import top.surgeqi.security.demo.bean.po.Role;

import java.util.List;

/**
 * <p><em>Created by qipp on 2020/7/1 15:36</em></p>
 * 权角色 Mapper 接口
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Repository
public interface RoleMapper extends BaseMapper<Role> {

    /**
     * 按照用户账户id查询角色
     *
     * @param accountId 用户账户id
     * @return java.util.List 角色列表结果
     * @author qipp
     */
    List<Role> selByUserId(Long accountId);
}
