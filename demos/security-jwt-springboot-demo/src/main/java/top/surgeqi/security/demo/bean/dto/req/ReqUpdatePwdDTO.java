package top.surgeqi.security.demo.bean.dto.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * <p><em>Created by qipp on 2020/10/14 17:30</em></p>
 * 用户更新密码 DTO
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Data
@ApiModel(value = "用户更新密码")
public class ReqUpdatePwdDTO {

    @NotNull(message = "密码为空")
    @ApiModelProperty("密码")
    private String password;

    @NotNull(message = "新密码为空")
    @ApiModelProperty(value = "新密码")
    private String newPassword;
}
