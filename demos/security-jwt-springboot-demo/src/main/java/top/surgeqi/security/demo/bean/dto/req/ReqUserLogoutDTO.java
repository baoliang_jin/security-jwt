package top.surgeqi.security.demo.bean.dto.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p><em>Created by qipp on 2020/10/6 9:41 上午</em></p>
 * 用户退出登录 DTO
 *
 * @author <a href ="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Data
@ApiModel(value = "用户退出登录")
public class ReqUserLogoutDTO {

    @ApiModelProperty(value = "令牌")
    private String accessToken;

    @ApiModelProperty(value = "刷新令牌")
    private String refreshToken;
}
