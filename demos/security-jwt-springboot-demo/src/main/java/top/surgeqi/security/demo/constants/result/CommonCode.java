package top.surgeqi.security.demo.constants.result;

import lombok.Getter;

/**
 * <p><em>Created by qipp on 2019/6/24 14:56.</em></p>
 * 通用的请求响应枚举
 * <p>包括{@link top.surgeqi.security.demo.bean.core.Result} 类的
 * code、subCode 两个级别的通用请求响应码、响应消息
 * <blockquote><pre>
 * 根据 <strong>SUB_</strong> 前缀区分
 * SUCCESS、FAIL... 对应{@link top.surgeqi.security.demo.bean.core.Result} 中第一级别 code、msg
 * SUB_SUCCESS、SUB_FAIL... 对应{@link top.surgeqi.security.demo.bean.core.Result} 中第二级别 subCode、subMsg
 * </pre></blockquote>
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 2.0.1
 */
@Getter
public enum CommonCode {

    /**
     * 请求成功
     */
    SUCCESS("SUCCESS","请求成功" ),

    /**
     * 内部业务错误
     */
    FAIL("FAIL","内部业务错误"),

    /**
     * 异常
     */
    ERROR("ERROR","内部业务异常"),

    /**
     * http 请求方式不支持异常
     */
    HTTP_METHOD_ERROR("HTTP_METHOD_ERROR","请求方式不支持"),



    /**
     * 业务请求成功
     */
    SUB_SUCCESS("OK", "业务请求成功"),

    /**
     * 参数校验失败
     */
    SUB_VERIFICATION_FAILED("VERIFICATION_FAILED","格式校验失败"),

    /**
     * 未定义业务失败
     */
    SUB_FAIL("UNDEFINED_FAIL", "未定义业务失败"),

    /**
     * 未知异常
     */
    UNKNOWN_ERROR("UNKNOWN_ERROR","未知异常"),

    /**
     * 入参校验异常
     */
    JSR303_ERROR("JSR303_ERROR","入参校验异常"),

    /**
     * 请求超时
     */
    FEIGN_TIMED_OUT("TIMED_OUT", "Feign调用请求超时"),

    /**
     * 认证相关异常
     */
    AUTH_ERROR("AUTH_ERROR", "认证相关异常"),
    ;

    /**
     * 通用响应码
     */
    private final String resCode;

    /**
     * 通用响应消息
     */
    private final String resMsg;

    CommonCode(String resCode, String resMsg) {
        this.resCode = resCode;
        this.resMsg = resMsg;
    }

    /**
     * 根据通用响应码返回通用响应消息
     * @param resCode 通用响应码
     * @return java.lang.String 通用响应消息
     * @author qipp
     */
    public static String getResMsgByResCode(String resCode) {
        CommonCode[] responseConstants = CommonCode.values();
        for (CommonCode ret : responseConstants) {
            if (ret.getResCode().equals(resCode)) {
                return ret.getResMsg();
            }
        }
        return "";
    }
}
