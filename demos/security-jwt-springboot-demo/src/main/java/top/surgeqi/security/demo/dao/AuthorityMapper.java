package top.surgeqi.security.demo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import top.surgeqi.security.demo.bean.po.Authority;

import java.util.List;

/**
 * <p><em>Created by qipp on 2020/7/1 15:36</em></p>
 * 权限 Mapper 接口
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Repository
public interface AuthorityMapper extends BaseMapper<Authority> {

    /**
     * 按角色id查询权限集合
     * @param roleId  角色id
     * @return java.util.List 权限结果
     * @author qipp
     */
    List<Authority> selByRoleId(Long roleId);
}
