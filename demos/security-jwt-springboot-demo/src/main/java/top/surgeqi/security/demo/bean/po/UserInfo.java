package top.surgeqi.security.demo.bean.po;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p><em>Created by qipp on 2020/10/1 11:11</em></p>
 * 用户信息
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UserInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId
    private Long id;

    /**
     * 账户id
     */
    private Long accountId;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * M:男 F:女 N:未知
     */
    private String sex;

    /**
     * 生日
     */
    private LocalDateTime birthday;

    /**
     * 头像
     */
    private String headPortrait;

    /**
     * 地址
     */
    private String addressCode;

    /**
     * 邮箱
     */
    private String email;

    /**
     * QQ号
     */
    private String qqNumber;

    /**
     * 微信号
     */
    private String wechatNumber;

    /**
     * 个性签名
     */
    private String personalizedSignature;

    /**
     * 0:否 1:是
     */
    private Boolean isDelete;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    /**
     * 活跃度
     */
    private Integer liveness;


}
