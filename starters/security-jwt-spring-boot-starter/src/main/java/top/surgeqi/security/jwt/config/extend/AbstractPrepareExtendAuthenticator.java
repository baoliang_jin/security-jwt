package top.surgeqi.security.jwt.config.extend;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import top.surgeqi.security.jwt.config.AbstractUserDetailsService;

/**
 * <p><em>Created by qipp on 2020/6/30 10:41</em></p>
 * 扩展认证器抽象类
 * <p>验证用户可用性并组合用户账户对象与角色权限</p>
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Slf4j
public abstract class AbstractPrepareExtendAuthenticator implements ExtendAuthenticator {

    /**
     * 用户详情Service
     */
    @Autowired
    protected AbstractUserDetailsService abstractUserDetailsService;


    /**
     * 验证用户可用性并组合用户账户对象与角色权限
     * <p>返回实际的认证类型</p>
     *
     * @param userDetails 用户详情对象
     * @return java.lang.String 认证类型默认为密码方式认证
     * @author qipp
     */
    protected UserDetails validateAndComposeUser(UserDetails userDetails) {

        // 验证用户可用性
        abstractUserDetailsService.validateUser(userDetails);

        // 组合用户账户对象与角色权限
        return abstractUserDetailsService.composeUserDetailsAndAuthority(userDetails);
    }
}
