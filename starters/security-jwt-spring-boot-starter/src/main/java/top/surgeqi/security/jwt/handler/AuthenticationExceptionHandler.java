package top.surgeqi.security.jwt.handler;

import top.surgeqi.security.jwt.exception.CustomerAuthenticationException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <p><em>Created by qipp on 2020/5/29 11:01</em></p>
 * 自定义通用认证异常处理器
 * <blockquote><pre>
 *     用户已过期
 *     用户已锁定
 *     用户未启用
 * </pre></blockquote>
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
public interface AuthenticationExceptionHandler {

    /**
     * 自定义通用认证异常以及子类异常处理器
     *
     * @param request                         HttpServletRequest
     * @param response                        HttpServletResponse
     * @param customerAuthenticationException 自定义通用认证异常以及子类
     * @throws IOException      IO异常
     * @throws ServletException Servlet 异常
     */
    void onAuthenticationException(HttpServletRequest request,
                                   HttpServletResponse response,
                                   CustomerAuthenticationException customerAuthenticationException) throws IOException, ServletException;
}
