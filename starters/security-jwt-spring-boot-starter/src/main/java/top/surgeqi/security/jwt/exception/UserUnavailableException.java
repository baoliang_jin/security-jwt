package top.surgeqi.security.jwt.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * <p><em>Created by qipp on 2020/6/30 12:45</em></p>
 * 自定义用户不可用异常类
 * <blockquote><pre>
 *     用户已过期
 *     用户已锁定
 *     用户未启用
 * </pre></blockquote>
 *
 * @author <a href="https://gitee.com/qipengpai">qipp</a>
 * @since 1.0.1
 */
@Slf4j
public class UserUnavailableException extends CustomerAuthenticationException {
    public UserUnavailableException(String msg) {
        super(msg);
        log.error("自定义用户不可用异常 msg -> {}", msg);
    }
}
